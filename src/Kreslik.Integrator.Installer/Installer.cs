﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Installer
{
    public class Installer
    {
        public static void Install(bool install, string[] assembliesToNgen)
        {
            ILogger logger = LogFactory.Instance.GetLogger("Installer");
            Console.WriteLine("Starting {0} action. Details will be in log files", install ? "Install" : "Uninstall");
            logger.Log(LogLevel.Info, "Starting {0} action. Details will be in log files", install ? "Install" : "Uninstall");

            Installer installer = new Installer();

            foreach (string assemblyToNgen in assembliesToNgen)
            {
                Console.WriteLine("Starting Ngen for [{0}]...", assemblyToNgen);
                logger.Log(LogLevel.Info, "Starting Ngen for [{0}]...", assemblyToNgen);
                string output = installer.StartNgen(install, assemblyToNgen);
                logger.Log(LogLevel.Info, output);
            }

            Console.WriteLine("{0} Perf counters...", install ? "Installing" : "Uninstalling");
            logger.Log(LogLevel.Info, "{0} Perf counters...", install ? "Installing" : "Uninstalling");
            bool success = installer.InstallPerfCounter(install, logger);
            Console.WriteLine("{0} Perf counters {1}.", install ? "Installing" : "Uninstalling", success ? "succeeded" : "probably failed");
            logger.Log(success ? LogLevel.Info : LogLevel.Warn, "{0} Perf counters...", install ? "Installing" : "Uninstalling");

            Console.WriteLine("DONE");
            logger.Log(LogLevel.Info, "DONE");
        }


        volatile bool processExited = false;

        public bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            try
            {
                //get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException)
            {
                isAdmin = false;
            }
            catch (Exception)
            {
                isAdmin = false;
            }
            return isAdmin;
        }

        public string StartNgen(bool install, string assemblyToNgen)
        {
            string netDirectory = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
            string ngenPath = System.IO.Path.Combine(netDirectory, "NGen.exe");
            //Boolean exists = System.IO.File.Exists(ngenPath);

            Process process = new Process();
            //process.StartInfo.FileName = "ping.exe";
            //process.StartInfo.Arguments = "www.google.com";
            process.StartInfo.FileName = ngenPath;
            process.StartInfo.Arguments = (install ? "install \"" : "uninstall \"") + assemblyToNgen + "\"";

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.CreateNoWindow = true;

            process.Start();

            StringBuilder sb = new StringBuilder();

            Task writeTask = Task.Factory.StartNew(() =>
            {
                while (!processExited)
                {
                    char c = (char)process.StandardOutput.Read();
                    Console.Write(c);
                    sb.Append(c);
                    System.Threading.Thread.Sleep(1);
                }

                string s = process.StandardOutput.ReadToEnd();
                Console.Write(s);
                sb.Append(s);
            });

            process.WaitForExit();
            processExited = true;

            writeTask.Wait();
            return sb.ToString();
        }

        public bool InstallPerfCounter(bool install, ILogger logger)
        {
            bool success = IntegratorPerformanceCounter.UninstallCounters(logger);
            if (install)
            {
                success &= IntegratorPerformanceCounter.InstallCountersIfNeeded(logger);
            }

            return success;
        }
    }
}
