﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.Installer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1 || args[0].StartsWith("-") || args[0].StartsWith("/"))
            {
                Console.WriteLine("Run Installer with one or more arguments - full path to assemblies to be ngened");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
            else
            {
                Installer.Install(true, args);
            }
        }
    }
}
